import React from 'react';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TemporaryDrawer from './TemporaryDrawer';
import './home.css';

class Home extends React.Component {
  state = {
    data: [],
    selectedData: {},
    TDrawer: false,
  }
  handleChangeSelectedData = (key, data, open) => {
    this.setState({[key]: data, TDrawer: open});
  }
  componentDidMount() {
    const tempData = [];
    fetch('https://student-management-api-1u3cd4j7s.now.sh/students')
      .then(response => response.json())
      .then(data => {
        for (let i = 0; i < data.length; i++) {
          if (tempData[data[i].class]) {
            if (tempData[data[i].class][data[i].section]) {
              tempData[data[i].class][data[i].section] = [...tempData[data[i].class][data[i].section], data[i]];
            } else {
              tempData[data[i].class] = {
                ...tempData[data[i].class],
                [data[i].section]: [data[i]],
              }
            }
          } else {
            tempData[data[i].class] = {
              [data[i].section]: [data[i]],
            }
          }
        }
        this.setState({ data: tempData });
      });
  }
  render() {
    const { data, selectedData = {}, TDrawer= false } = this.state;
    return (
      <div className={"mainWrapper"}>
        {data.map((d, i) =>
          <div
            key={Math.random()}
            className="classWrapper"
          >
            <li>{`Class ${i}`}</li>
            {Object.keys(d).map(key =>
              <div
                key={Math.random()}
                className="sectionWrapper"
              >
                <li>{`Section ${key}`}</li>
                <div className="dataWrapper">
                  {d[key].map(d1 =>
                    <div
                      key={Math.random()}
                      className={"nameWrapper"}
                      onClick={() => this.handleChangeSelectedData('selectedData', d1, true)}
                    >
                      <Tooltip title={getCard(d1)}>
                        <Button variant="contained">{d1.name}</Button>
                      </Tooltip>
                    </div>
                  )}
                </div>
              </div>
            )}
          </div>
        )}
        <TemporaryDrawer
          open={TDrawer}
          selectedData={selectedData}
          toggleDrawer={() => this.handleChangeSelectedData('selectedData', {}, false)}
        />
      </div>
    )
  };
}

const getCard = (data) => {
  return (
    <div className={"cardWrapper"}>
      <div className="CardData">
      <Grid container item xs={12} spacing={1}>
        <Grid item xs={6}>
          <label>Name:</label>
        </Grid>
        <Grid item xs={6}>
          <span>{data.name}</span>
        </Grid>
      </Grid>
      </div>
      <div className="CardData">
      <Grid container item xs={12} spacing={3}>
        <Grid item xs={6}>
            <label>Age:</label>
        </Grid>
        <Grid item xs={6}>
          <span>{data.age}</span>
        </Grid>
      </Grid>
      </div>
      <div className="CardData">
        <Grid container item xs={12} spacing={3}>
          <Grid item xs={6}>
            <label>Gender:</label>
          </Grid>
          <Grid item xs={6}>
            <span>{data.gender}</span>
          </Grid>
        </Grid>
      </div>
      <div className="CardData">
      <Grid container item xs={12} spacing={3}>
        <Grid item xs={6}>
          <label>Sport:</label>
        </Grid>
        <Grid item xs={6}>
          <span>{data.sports.toString()}</span>
        </Grid>
      </Grid>
      </div>
    </div>
  );
}

export default Home;
