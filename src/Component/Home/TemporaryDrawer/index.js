import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import Grid from '@material-ui/core/Grid';
import { IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  paper: {
    background: '#585858',
    color: 'white'
  }
});

const TemporaryDrawer = (props) => {
  const styles = useStyles();
  const { open, toggleDrawer, selectedData= {} } = props;
  return (
    <div>
      <Drawer
        anchor={'right'}
        open={open}
        onClose={toggleDrawer}
        classes={{ paper: styles.paper }}
      >
        <Grid container item alignItems="flex-start">
          <IconButton variant="contained" onClick={toggleDrawer}>
            <CloseIcon />
          </IconButton>
        </Grid>
        <div className={"cardWrapper TDrawerData"}>
          <div className="CardData">
            <Grid container item xs={12} spacing={1}>
              <Grid item xs={6}>
                <label>Name:</label>
              </Grid>
              <Grid item xs={6}>
                <span className="bgWhite" >{selectedData.name}</span>
              </Grid>
            </Grid>
          </div>
          <div className="CardData">
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={6}>
                <label>Age:</label>
              </Grid>
              <Grid item xs={6}>
                <span>{selectedData.age}</span>
              </Grid>
            </Grid>
          </div>
          <div className="CardData">
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={6}>
                <label>Gender:</label>
              </Grid>
              <Grid item xs={6}>
                <span>{selectedData.gender}</span>
              </Grid>
            </Grid>
          </div>
          <div className="CardData">
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={6}>
                <label>Sport:</label>
              </Grid>
              <Grid item xs={6}>
                <span className="bgWhite" >{selectedData.sports ? selectedData.sports.toString(): ''}</span>
              </Grid>
            </Grid>
          </div>
        </div>
      </Drawer>
    </div>
  );
}

export default TemporaryDrawer;